## PACKAGE
- npm install axios body-parser cors dotenv express express-validator express-rate-limit mongoose nodemon --save


## FLOW
index.js -> loaders/index.js -> express.js -> app/routes/api.js -> app/controller/weather -> app/services/openweatherapi.js -> output data for user


## FOLDER STRU
- APP: APPLICATION
  - CONFIG: CONFIG
  - CONTROLLER : BUSINESS LOGIC
  - ROUTES : ROUTES
  - SERVICES: BUSSINESS LOGIC / EXTERNAL SERVICES
  - MODEL: model
- LOADERS:
  - EXPRESS : SERVER
  - MONGOOSE: CONNECT TO DB
- index.js (entry point)
- .env (DONT NOT SUBMIT)
- .env.example (submit to git,no value should be submitted)


## RESTFUL API STANDARDED

![Scheme](https://laraveldaily.com/wp-content/uploads/2015/07/0707_laravel_restful.png)